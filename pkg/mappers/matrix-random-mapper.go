package mappers

// MapperIntToFloat64 Function to convert []int to []float64
func MapperIntToFloat64(intSlice []int) []float64 {
	floatSlice := make([]float64, len(intSlice))
	for i, v := range intSlice {
		floatSlice[i] = float64(v)
	}
	return floatSlice
}

package routes

import (
	"github.com/gofiber/fiber/v2"
	"matrix-api/pkg/controllers"
	"matrix-api/pkg/middlewares"
)

func MatrixRoute(a *fiber.App) {
	route := a.Group("/matrix/api/v1")
	route.Use(middlewares.CheckSession)
	route.Post("/rotate", controllers.RotateMatrix)
	route.Post("/qr", controllers.QRMatrix)
}

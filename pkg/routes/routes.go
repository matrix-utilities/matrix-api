package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
	_ "matrix-api/docs"
	"matrix-api/pkg/controllers"
)

func ConfigRoutes(app *fiber.App) {
	// swagger
	route := app.Group("/swagger")
	route.Get("*", swagger.HandlerDefault)

	app.Get("/login", controllers.Login)

	// matrix routes
	MatrixRoute(app)
}

package middlewares

import (
	"errors"
	"fmt"
	"matrix-api/pkg/resources"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
)

func CheckSession(c *fiber.Ctx) error {
	// Obtiene el token del encabezado de autorización
	response := resources.ResponseHTTP{}
	tokenString := c.Get("Authorization")
	if tokenString == "" {
		response.ErrorWithCode("99", errors.New("Token de autenticación no proporcionado"))
		return c.Status(fiber.StatusUnauthorized).JSON(response)
	}

	// Verifica y decodifica el token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Verifica el algoritmo de firma
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("método de firma no válido")
		}
		return []byte(os.Getenv("JWT_KEY")), nil
	})
	if err != nil {
		response.ErrorWithCode("99", err)
		return c.Status(fiber.StatusUnauthorized).JSON(response)
	}

	// Verifica si el token es válido
	if !token.Valid {
		response.ErrorWithCode("99", errors.New("Token de autenticación inválido"))
		return c.Status(fiber.StatusUnauthorized).JSON(response)
	}

	// Continúa con el siguiente middlewares o controlador
	return c.Next()
}

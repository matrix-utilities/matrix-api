package services

import (
	"gonum.org/v1/gonum/mat"
	"math/rand"
	"matrix-api/pkg/mappers"
	"time"
)

type MatrixService struct {
}

func NewMatrixService() MatrixService {
	return MatrixService{}
}

func (m MatrixService) Rotate(matrix [][]float64) [][]float64 {
	n := len(matrix)

	for x := 0; x < n/2; x++ {
		for y := x; y < n-x-1; y++ {
			temp := matrix[x][y]
			matrix[x][y] = matrix[y][n-1-x]
			matrix[y][n-1-x] = matrix[n-1-x][n-1-y]
			matrix[n-1-x][n-1-y] = matrix[n-1-y][x]
			matrix[n-1-y][x] = temp
		}
	}
	return matrix
}

func (m MatrixService) Factorized(matrix [][]float64) ([][]float64, [][]float64) {
	rows := len(matrix)
	cols := len(matrix[0])
	data := make([]float64, rows*cols)
	for i := 0; i < rows; i++ {
		copy(data[i*cols:(i+1)*cols], matrix[i])
	}
	A := mat.NewDense(rows, cols, data)

	// Perform QR factorization
	var qr mat.QR
	qr.Factorize(A)

	// Extract Q and R matrices
	Q := mat.NewDense(rows, rows, nil)
	qr.QTo(Q)
	R := mat.NewDense(rows, cols, nil)
	qr.RTo(R)

	// Convert Q and R matrices to slice of slices
	qData := make([][]float64, rows)
	rData := make([][]float64, rows)
	for i := 0; i < rows; i++ {
		qData[i] = Q.RawRowView(i)
		rData[i] = R.RawRowView(i)
	}
	return qData, rData
}

func (m MatrixService) Random() [][]float64 {
	var array [][]float64

	rand.Seed(time.Now().Unix())

	x := rand.Intn(6)
	if x <= 1 {
		x = 3
	}
	for i := 0; i < x; i++ {
		array = append(array, mappers.MapperIntToFloat64(rand.Perm(50)[:x]))
	}
	return array
}

package controllers

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"matrix-api/pkg/resources"
	"os"
	"time"
)

// Login handles the GET request to generate JWT token.
// @Summary Generate a JWT token using a random ID
// @Description This endpoint generates a JWT token based on a provided random ID. The ID is passed as a query parameter. If the ID is not provided, a 400 error is returned. Upon successful generation, the token is returned in the response. If there's an issue creating the token, a 500 error is returned.
// @Tags login
// @Produce json
// @Param id query string true "Random ID"
// @Success 200 {object} resources.ResponseHTTP "A successful response example: {\"code\": \"01\", \"data\": {\"token\": \"your_jwt_token\"}}"
// @Failure 400 {object} resources.ResponseError "An error response example: {\"code\": \"99\", \"error\": \"ID is required\"}"
// @Failure 500 {object} resources.ResponseError "An error response example: {\"code\": \"99\", \"error\": \"Could not create token\"}"
// @Router /login [get]
func Login(c *fiber.Ctx) error {
	response := resources.ResponseHTTP{}
	id := c.Query("id", "")
	if id == "" {
		response.ErrorWithCode("99", errors.New("ID is required"))
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	expirationTime := time.Now().Add(10 * time.Minute)
	claims := &resources.Claims{
		ID: id,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_KEY")))
	if err != nil {
		response.ErrorWithCode("99", err)
		return c.Status(fiber.StatusInternalServerError).JSON(response)
	}
	response.Success(fiber.Map{"token": tokenString})
	return c.JSON(response)
}

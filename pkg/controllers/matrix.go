package controllers

import (
	"github.com/gofiber/fiber/v2"
	"matrix-api/pkg/resources"
	"matrix-api/pkg/services"
)

// RotateMatrix handles the POST request to rotate any NxN matrix.
// @Description Rotates a matrix of any dimension NxN provided in the request body.
// This endpoint processes the matrix and returns the rotated version, or an error
// if the input matrix is not correctly formatted or cannot be parsed.
// @Summary Rotate a matrix
// @Tags matrix
// @Accept json
// @Produce json
// @Param Authorization header string true "<JWT>"
// @Param matrix body resources.Matrix true "The matrix to be rotated. Example: {\"array\": [[0, 0, 1], [1, 0, 0], [0, 1, 0]]}" example({"array": [[1,2,3], [4,5,6], [7,8,9]]})
// @Success 200 {object} resources.ResponseRotate "A successful response example: {\"code\": \"01\", \"data\": {\"array\": [[9, 6, 3], [8, 5, 2], [7, 4, 1]]}}"
// @Failure 400 {object} resources.ResponseError "An error response example: {\"code\": \"99\", \"error\": \"Invalid matrix input\", \"data\": {\"message\": \"Matrix dimensions are incorrect\", \"example\": [[9, 6, 3], [8, 5, 2], [7, 4, 1]]}}"
// @Router /matrix/api/v1/rotate [post]
func RotateMatrix(c *fiber.Ctx) error {
	response := resources.ResponseHTTP{}
	matrix := resources.MatrixResponse{}
	request := resources.Matrix{}
	service := services.NewMatrixService()

	if err := c.BodyParser(&request); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
			"example": service.Random(), // Provide an example of a valid matrix.
		}
		response.ErrorWithData(err, data)
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	if err := request.Validate(); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
			"example": service.Random(), // Provide an example of a valid matrix.
		}
		response.ErrorWithData(err, data)
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	// Rotate the matrix using the service logic.
	rotatedMatrix := service.Rotate(request.Array)

	matrix.Array = rotatedMatrix

	// Set the response data with the rotated matrix and return a 200 OK status.
	response.Success(matrix)
	return c.Status(fiber.StatusOK).JSON(response)
}

// QRMatrix handles the POST request to Factorization QR on any NxN matrix.
// @Description Factorize a NxN matrix provided in the request body.
// This endpoint processes the matrix and returns the factorization, or an error
// if the input matrix is not correctly formatted or cannot be parsed.
// @Summary Rotate a matrix
// @Tags matrix
// @Accept json
// @Produce json
// @Param Authorization header string true "<JWT>"
// @Param matrix body resources.Matrix required "The matrix to be factorized. Example: {\"array\": [[0, 0, 1], [1, 0, 0], [0, 1, 0]]}" example({"array": [[0, 0, 1], [1, 0, 0], [0, 1, 0]]})
// @Success 200 {object} resources.ResponseQR "A successful response example: {\"code\": \"01\", \"data\": {\"Q\": [[0, 0, 1], [1, 0, 0], [0, 1, 0]], \"R\": [[1, 2, 3], [0, 4, 5], [0, 0, 6]]}}"
// @Failure 400 {object} resources.ResponseError "An error response example: {\"code\": \"99\", \"error\": \"Invalid matrix input\", \"data\": {\"message\": \"Matrix dimensions are incorrect\", \"example\": [[0, 0, 1], [1, 0, 0], [0, 1, 0]]}}"
// @Router /matrix/api/v1/qr [post]
func QRMatrix(c *fiber.Ctx) error {
	response := resources.ResponseHTTP{}
	matrix := resources.MatrixQRResponse{}
	request := resources.Matrix{}
	service := services.NewMatrixService()

	if err := c.BodyParser(&request); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
			"example": service.Random(), // Provide an example of a valid matrix.
		}
		response.ErrorWithData(err, data)
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	if err := request.Validate(); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
			"example": service.Random(), // Provide an example of a valid matrix.
		}
		response.ErrorWithData(err, data)
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	// Factorized the matrix using the service logic.
	matrix.Q, matrix.R = service.Factorized(request.Array)

	// Set the response data with the factorized matrix and return a 200 OK status.
	response.Success(matrix)
	return c.Status(fiber.StatusOK).JSON(response)
}

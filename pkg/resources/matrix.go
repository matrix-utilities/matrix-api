package resources

import "errors"

type Matrix struct {
	Array [][]float64 `json:"array"`
}

func (m Matrix) Validate() error {
	err := errors.New("invalid matrix format. Must be a NxN matrix")
	if m.Array == nil {
		return errors.New("invalid matrix format. Matrix cannot be empty")
	}
	n := len(m.Array)
	if n < 1 {
		return err
	}
	for i := 0; i < n; i++ {
		if len(m.Array[i]) != n {
			return err
		}
	}
	return nil
}

package resources

type ResponseHTTP struct {
	Error *string     `json:"error,omitempty"`
	Code  string      `json:"code"`
	Data  interface{} `json:"data"`
}

type ResponseRotate struct {
	Error *string `json:"error,omitempty"`
	Code  string  `json:"code"`
	Data  struct {
		Array [][]float64 `json:"array"`
	} `json:"data"`
}

type ResponseQR struct {
	Error *string `json:"error,omitempty"`
	Code  string  `json:"code"`
	Data  struct {
		Q [][]float64 `json:"Q"`
		R [][]float64 `json:"R"`
	} `json:"data"`
}

type ResponseError struct {
	Error *string `json:"error,omitempty"`
	Code  string  `json:"code"`
	Data  struct {
		Message string      `json:"message,omitempty"`
		Example [][]float64 `json:"example,omitempty"`
	} `json:"data"`
}

func (r *ResponseHTTP) ResponseError(err error) {
	r.Code = "99"
	e := err.Error()
	r.Error = &e
}

func (r *ResponseHTTP) ErrorWithData(err error, data interface{}) {
	r.Code = "99"
	e := err.Error()
	r.Error = &e
	r.Data = data
}

func (r *ResponseHTTP) ErrorWithCode(code string, err error) {
	r.Code = code
	e := err.Error()
	r.Error = &e
}

func (r *ResponseHTTP) Success(data interface{}) {
	r.Code = "01"
	r.Data = data
}

package resources

type MatrixQRResponse struct {
	Q [][]float64 `json:"Q"`
	R [][]float64 `json:"R"`
}

type MatrixResponse struct {
	Array [][]float64 `json:"array"`
}

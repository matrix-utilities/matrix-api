package test

import (
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"matrix-api/cmd/test"
	"matrix-api/pkg/resources"
)

var payload string = "{\"array\":[[1,3,5],[1,3,1],[2,-1,7]]}"
var matrix [][]float64 = [][]float64{{1, 3, 5}, {1, 3, 1}, {2, -1, 7}}
var rotate [][]float64 = [][]float64{{5, 1, 7}, {3, 3, -1}, {1, 1, 2}}
var q [][]float64 = [][]float64{
	{-0.40824829046386313, -0.577350269189626, -0.7071067811865476},
	{-0.4082482904638631, -0.5773502691896257, 0.7071067811865475},
	{-0.8164965809277261, 0.5773502691896257, -1.6653345369377348e-16},
}
var r [][]float64 = [][]float64{
	{-2.449489742783178, -1.6329931618554525, -8.164965809277263},
	{0, -4.0414518843273814, 0.5773502691896248},
	{0, 0, -2.828427124746191},
}

func TestRotate(t *testing.T) {
	app := test.InitializeConfigTest()

	req := httptest.NewRequest("POST", "/matrix/api/v1/rotate", strings.NewReader(payload))
	req.Header.Set("Content-Type", "application/json")
	res, err := app.Test(req, -1)

	assert.NoError(t, err, "Request Error!")
	assert.Equal(t, 200, res.StatusCode, "Status Code should be 200")

	body, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err, "Error reading response body")

	var response resources.ResponseRotate
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err, "Error unmarshalling response body")

	assert.Equal(t, 3, len(response.Data.Array), "Matrix should have 3 rows")
	assert.Equal(t, 3, len(response.Data.Array[0]), "Matrix should have 3 columns")
	assert.Equal(t, rotate, response.Data, "Matrix values do not match expected rotation")
}

func TestFactorize(t *testing.T) {
	app := test.InitializeConfigTest()

	req := httptest.NewRequest("POST", "/matrix/api/v1/qr", strings.NewReader(payload))
	req.Header.Set("Content-Type", "application/json")
	res, err := app.Test(req, -1)

	assert.NoError(t, err, "Request Error!")
	assert.Equal(t, 200, res.StatusCode, "Status Code should be 200")

	body, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err, "Error reading response body")

	var response resources.ResponseQR
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err, "Error unmarshalling response body")

	assert.Equal(t, 3, len(response.Data.Q), "Q Matrix should have 3 rows")
	assert.Equal(t, 3, len(response.Data.Q[0]), "Q Matrix should have 3 columns")
	assert.Equal(t, q, response.Data.Q, "Q Matrix values do not match expected values")
	assert.Equal(t, 3, len(response.Data.R), "R Matrix should have 3 rows")
	assert.Equal(t, 3, len(response.Data.R[0]), "R Matrix should have 3 columns")
	assert.Equal(t, r, response.Data.R, "R Matrix values do not match expected values")
}

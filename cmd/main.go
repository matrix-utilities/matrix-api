package main

import (
	"encoding/json"
	"fmt"
	"matrix-api/cmd/config"
	"matrix-api/cmd/server"
	_ "matrix-api/docs"
	"matrix-api/pkg/middlewares"
	"matrix-api/pkg/routes"
)

// @title           Swagger Matrix API
// @version         1.0
// @description     This is a Matrix API to rotate and factorize matrix NxN.
// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html
// @host      127.0.0.1:8080
// @BasePath
func main() {
	config.InitializeBasicConfig()
	app := server.InitializeServer()

	// Middlewares
	middlewares.FiberMiddleware(app)

	// Routes
	routes.ConfigRoutes(app)

	if config.IsLocal() {
		// Print the router stack in JSON format
		data, _ := json.MarshalIndent(app.GetRoutes(true), "", "  ")
		fmt.Println(string(data))
	}

	server.RunServer(app)
}

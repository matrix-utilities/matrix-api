package server

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"log"
	env "matrix-api/cmd/config"
	_ "matrix-api/docs"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"
)

func InitializeServer() *fiber.App {
	var timeout = os.Getenv("server.timeout")
	var name = os.Getenv("server.name")
	readTimeoutSecondsCount, _ := strconv.Atoi(timeout)

	// Return Fiber configuration.
	config := fiber.Config{
		ServerHeader: name,
		ReadTimeout:  time.Second * time.Duration(readTimeoutSecondsCount),
	}

	return fiber.New(config)
}

func generateUrl() string {
	var host = os.Getenv("server.host")
	var port = os.Getenv("server.port")
	return fmt.Sprintf("%s:%s", host, port)
}

func startServerWithGracefulShutdown(a *fiber.App) {
	// Create channel for idle connections.
	idleConnsClosed := make(chan struct{})

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt) // Catch OS signals.
		<-sigint

		// Received an interrupt signal, shutdown.
		if err := a.Shutdown(); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("Oops... Server is not shutting down! Reason: %v", err)
		}

		close(idleConnsClosed)
	}()

	// Run server.
	if err := a.Listen(generateUrl()); err != nil {
		log.Printf("Oops... Server is not running! Reason: %v", err)
	}

	<-idleConnsClosed
}

func startServer(a *fiber.App) {
	// Build Fiber connection URL.

	// Run server.
	if err := a.Listen(generateUrl()); err != nil {
		log.Printf("Oops... Server is not running! Reason: %v", err)
	}
}

// RunServer Start server (with or without graceful shutdown).
func RunServer(a *fiber.App) {
	appEnv := os.Getenv("APP_ENV")
	if strings.EqualFold(appEnv, env.Production) {
		startServerWithGracefulShutdown(a)
	} else {
		startServer(a)
	}
}

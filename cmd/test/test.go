package test

import (
	"github.com/gofiber/fiber/v2"
	"matrix-api/cmd/config"
	"matrix-api/cmd/server"
	"matrix-api/pkg/middlewares"
	"matrix-api/pkg/routes"
)

func InitializeConfigTest() *fiber.App {

	config.ReadConfigFile(config.RootPath)
	app := server.InitializeServer()
	// Middlewares.
	middlewares.FiberMiddleware(app) // cors  --

	// Routes.
	routes.ConfigRoutes(app) // Register routes for app.

	return app
}


# Api en Go para rotar y/o factorizar matriz NxN

- Dado un array NxN enviado por post se retornara un array de la misma dimensión pero rotado en sentido antihorario 90 grados.
- Dado un array NxN enviado por post se retornara la factorizacion QR.

## Ejecución:
1. Clonar repositorio
2. Instalar las depentencias utilizadas: "go mod download"
3. Ejecutar con el comando go run main.go

### Variables de entorno:

```
PORT=8080
HOST=0.0.0.0
JWT_KEY=test-123
```

### Para ejecutar en local:

```bash
swag init -g cmd/main.go --output docs
go run cmd/main.go
```
La url de swagger en local: [http://127.0.0.1:8080/swagger](http://127.0.0.1:8080/swagger)

### Para ejecutar en prod:

```bash
swag init -g cmd/main.go --output docs
APP_ENV=prod go run cmd/main.go
```

## Despliegue con Docker:
1. cd /path/to/DockerfileFolder
2. docker build --pull --rm -f "Dockerfile" -t matrix-api:1.0 "."
3. docker run -p 8080:8080 matrix-api:1.0

## Despliegue con gcloud (Example):

```sh
gcloud.cmd builds submit --tag us-east4-docker.pkg.dev/project/matrix-utilities/project:dev .
```

## Author
By [Orlando A. Yepes](https://www.linkedin.com/in/orlando-andr%C3%A9s-yepes-miquilena-9649544a/)
